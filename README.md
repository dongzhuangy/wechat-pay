# WehatPay

> 可简单调用就能实现的 微信支付 依赖包

## 已实现功能

- JSPay
- H5Pay
- Native

## 安装方式
```shell script
composer require dongzhuangy/wechat-pay
```

## 使用方式

### `Config` 配置项
```php
$config = [
    'appId' => 'testAppId',
    'appKey' => 'e4601aea9d95518629be6a6eb9a7f6ee',
    'mchId' => 'testMchId',
    'apiKey' => 'gkpgowe2ibuyyiqgvll5lvndgmzhu4zj'
];
```
> 这里解释一下，`appKey` 是用来获取 `openid` 的时候使用的，若是你的项目中 本来 就已经获取到的话，这里可以没有 `appKey`

### `JsPay` 公众号网页支付 

> 已有openid 可直接跳过 前面 2 步

1、获取授权跳转链接
```php
use dongzhuangy\WechatPay\JsPay;
$authBackUrl = 'http://www.test.com/payment-jspay.php'; //授权之后 会跳转回设定好的地址
$stateStr = 'test'; //携带数据，微信授权完成后，会将此数据包原样待会
$pay = JsPay::create($config);
$authUrl = $pay->_createOauthUrlForCode(urlencode($authBackUrl),$stateStr);
// 跳转去 这个 生成的地址进行授权，授权之后 会跳转回设定好的地址
```
2、从页面参数获取 `code` 并通过 `code` 获取 用户的 `openid`
> `payment-jspay.php`

```php
use dongzhuangy\WechatPay\JsPay;
$pay = JsPay::create($config);
$open = $pay->getOpenid();
if($open['code'] === $pay::CODE_SUCCESS){
    $openId = $open['data'];
}
```
3、发起支付，创建一个微信订单
```php
use dongzhuangy\WechatPay\JsPay;
$package = JsPay::create($config)->createJsBizPackage([
    'attach' => 'attach', // 订单数据包（字符串），异步返回回原样携带
    'title' => '支付测试', // 订单展示标题
    'open' => 'openid',   // 用户在公众号上的 openid
    'order' => '订单编号', // 你的订单编号
    'amount' => '支付金额',// 单位 元
    'notify' => 'http://www.notify.com/payment/notify',
]);
```

### `H5Pay` h5手机网页支付

```php
use dongzhuangy\WechatPay\H5Pay;
$package = H5Pay::create($config)->createJsBizPackage([
    'attach' => 'attach', // 订单数据包（字符串），异步返回回原样携带
    'title' => '支付测试', // 订单展示标题
    'order' => '订单编号', // 你的订单编号
    'amount' => '支付金额',// 单位 元
    'notify' => 'http://www.notify.com/payment/notify', // 支付成功之后的 异步通知地址
    'scene_info' => [
        'h5_info' => [
            'type' => 'Wap', 
            'wap_url' => 'http://www.test.com', 
            'wap_name' => '支付测试'
        ]
    ]
]);

var_dump($package);
```

### `Native` 网页扫码支付

```php
use dongzhuangy\WechatPay\Native;
$pack = Native::create($config)->createJsBizPackage([
    'attach' => 'attach', // 订单数据包（字符串），异步返回回原样携带
    'title' => '支付测试', // 订单展示标题
    'order' => 'native'.time(), // 你的订单编号
    'amount' => 0.01,// 单位 元
    'time_expire' => 6, // 订单超时时间（分钟），取值范围为[6,30]，区间外默认 6 分钟
    'notify' => 'http://www.notify.com/payment/notify', // 支付成功之后的 异步通知地址
]);
var_dump($package);
```

.... 更多微信支付方式敬请期待

### `Notify` 异步回调处理
```php
use dongzhuangy\WechatPay\Notify;
$notify = Notify::create($config);

// validateAndGetPackage 会直接验证签名，签名不通过的话会直接到 '验证失败' 的逻辑
if($package = $notify->validateAndGetPackage()){
    echo "验证成功";
    // 着这里做支付成功之后的数据处理（签名已经验证过了）
    var_dump($package);
    //...
    // 主动通知微信，当前订单数据已经处理成功，可以停止通知了
    $notify->returnOkToWechat();
}else{
    echo "验证失败";
}
```

### `Closer` 关闭订单
```php
use dongzhuangy\WechatPay\Closer;
$closer = Closer::create($config);
$rest = $closer->doClose('order-no');
if($rest['code'] === 200){
    // 订单关闭成功
}else{
    // 订单关闭失败
    print_r($rest);
}
```