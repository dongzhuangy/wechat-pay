<?php

namespace dongzhuangy\WechatPay;

use dongzhuangy\WechatPay\lib\WechatPay;

class H5Pay extends WechatPay
{

    /**
     * 预生成 订单创建数据包
     * @param array $params
     * @return array
     * @throws \Exception
     * @author: dongzhuangy 2021/4/27 3:33 下午
     */
    public function makeOrderParams(array $params): array
    {
        if(!isset($params['scene_info'])){
            throw new \Exception('params 中缺少 scene_info 参数');
        }
        return [
            'spbill_create_ip' => $this->getUserIp(),
            'scene_info' => json_encode(@$params['scene_info']),
            'trade_type' => 'MWEB',
        ];
    }

    /**
     * 后处理 wechat订单数据包
     * @param $packageXml
     * @return array|mixed
     */
    public function afterTreatment($packageXml): array
    {
        return $this->xmlToArray($packageXml);
    }
}