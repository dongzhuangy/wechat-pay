<?php

namespace dongzhuangy\WechatPay;

use dongzhuangy\WechatPay\lib\WechatPay;

class Native extends WechatPay
{

    /**
     * 预生成 订单创建数据包
     * @param array $params
     * @return array
     * @throws \Exception
     * @author: dongzhuangy 2021/4/27 3:33 下午
     */
    public function makeOrderParams(array $params): array
    {
        $effectiveTime = @$params['time_expire'];
        if(empty($effectiveTime) || $effectiveTime < 6 || $effectiveTime > 30){
            $effectiveTime = 6;
        }
        $expire = strtotime("+{$effectiveTime} minute",time());
        return [
            'trade_type' => 'NATIVE',
            'time_expire' => date('YmdHis',$expire)
        ];
    }

    /**
     * 后处理 wechat订单数据包
     * @param $packageXml
     * @return array|mixed
     */
    public function afterTreatment($packageXml): array
    {
        return $this->xmlToArray($packageXml);
    }

}