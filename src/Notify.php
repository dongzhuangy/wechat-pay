<?php

namespace dongzhuangy\WechatPay;

use dongzhuangy\WechatPay\lib\Wechat;

class Notify extends Wechat
{

    /**
     * 验证并解析 数据包
     * @return array|string[]|null
     * @author: dongzhuangy 2021/4/24 8:40 下午
     */
    public function validateAndGetPackage(): ?array
    {
        $xml = file_get_contents('php://input');
        if(empty($xml)){
            return null;
        }
        $package = $this->xmlToArray(simplexml_load_string($xml));
        $packageSign = $package['sign'];
        unset($package['sign']);
        if($packageSign === $this->getSign($package,$this->apiKey)){
            return $package;
        }
        return null;
    }

    /**
     * 返回处理成功给 微信，告诉它不用再次通知当前订单了（不告知的话，微信还会一直通知的[频次越来越低]）
     * @author: dongzhuangy 2021/4/28 3:56 下午
     */
    public function returnOkToWechat(): void
    {
        header('Content-type: text/xml; charset=UTF-8');
        $xml = $this->arrayToXml(['return_code' => 'SUCCESS', 'return_msg' => 'OK']);
        die($xml);
    }

}